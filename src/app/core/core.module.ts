import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './layouts/components/header/header.component';
import { FooterComponent } from './layouts/components/footer/footer.component';
import { MainPageComponent } from './layouts/main-page/main-page.component';
import {IconsModule} from "../icons/icons.module";
import {HttpClientModule} from "@angular/common/http";



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    MainPageComponent
  ],
  imports: [
    CommonModule,
    IconsModule,
    HttpClientModule
  ],
  exports: [
    IconsModule,
    FooterComponent,
    HeaderComponent,
    MainPageComponent
  ]
})
export class CoreModule { }
