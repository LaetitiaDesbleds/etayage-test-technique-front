import {CoordonneeI} from './coordonnee-i';

export interface OrganismeI {
  id: number;
  nomOrganisme: string;
  sigle: string;
  complementNom: string;
  adressePostale: string;
  complementAdresse: string;
  boitePostale: string;
  codePostal: number;
  commune: string;
  tel: string;
  tel2: string;
  email: string;
  url: string;
  descriptionActivite: string;
  ageEnClair: string;
  niveauActivite: string;
  momentActivite: string;
  tarif: string;
  contactActivite: string;
  lieuActivite: string;
  activite: string;
  paiementsAcceptes: string;
  age: string;
  niveau: string;
  communeOuQuartierDeLactivite: string;
  coordonneeId: number;
  coordonnee: CoordonneeI;
}
