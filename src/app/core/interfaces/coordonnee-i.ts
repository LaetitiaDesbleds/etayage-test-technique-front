export interface CoordonneeI {
  id: number;
  communeOuQuartierDeLactivite: string;
  latitude: number;
  longitude: number;
}
