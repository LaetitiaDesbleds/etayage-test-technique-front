import {OrganismeI} from "../interfaces/organisme-i";
import {Coordonnee} from "./coordonnee";

export class Organisme implements OrganismeI {
  id = 0;
  nomOrganisme= '';
  sigle= '';
  complementNom= '';
  adressePostale= '';
  complementAdresse= '';
  boitePostale= '';
  codePostal= 0;
  commune= '';
  tel = '';
  tel2= '';
  email= '';
  url= '';
  descriptionActivite= '';
  ageEnClair= '';
  niveauActivite= '';
  momentActivite= '';
  tarif= '';
  contactActivite= '';
  lieuActivite= '';
  activite= '';
  paiementsAcceptes= '';
  age= '';
  niveau= '';
  coordonneeId = 0;
  communeOuQuartierDeLactivite= '';

  coordonnee = new Coordonnee();

  /**
   *
   * param obj objet partiel
   */
  constructor(obj?: Partial<Organisme>) {
    if (obj) {
      Object.assign(this, obj);
    }

    if (!(this.coordonnee instanceof Coordonnee)) {
      this.coordonnee = new Coordonnee(this.coordonnee);
    }
  }

}
