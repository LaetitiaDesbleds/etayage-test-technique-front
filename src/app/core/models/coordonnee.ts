import {CoordonneeI} from "../interfaces/coordonnee-i";

export class Coordonnee implements CoordonneeI {
  id = 0;
  communeOuQuartierDeLactivite= '';
  latitude = 0;
  longitude = 0;

  /**
   *
   * param obj objet partiel
   */
  constructor(obj?: Partial<Coordonnee>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }
}
