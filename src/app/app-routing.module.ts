import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: 'activity-page', loadChildren:
      () => import('./features/activity-page/activity-page.module')
        .then((module_) => module_.ActivityPageModule)
  },
  {path: '', pathMatch: 'full', redirectTo: 'activity-page'},
];
@NgModule({
  imports: [RouterModule.forRoot(routes,
    {preloadingStrategy: PreloadAllModules, onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
