import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MailComponent } from './mail/mail.component';
import { LinkedinComponent } from './linkedin/linkedin.component';
import { GitlabComponent } from './gitlab/gitlab.component';
import { ArrowComponent } from './arrow/arrow.component';



@NgModule({
    declarations: [
        MailComponent,
        LinkedinComponent,
        GitlabComponent,
        ArrowComponent
    ],
  exports: [
    MailComponent,
    LinkedinComponent,
    GitlabComponent,
    ArrowComponent
  ],
    imports: [
        CommonModule
    ]
})
export class IconsModule { }
