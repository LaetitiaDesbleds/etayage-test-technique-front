import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Coordonnee} from "../../../core/models/coordonnee";

@Injectable({
  providedIn: 'root'
})
export class CoordonneService {

  private url = 'http://localhost:3001/coordonnees';

  constructor(private http: HttpClient) { }

  getCoordonnees(): Observable<Coordonnee[]> {
    return this.http.get<Coordonnee[]>(this.url);
  }
}
