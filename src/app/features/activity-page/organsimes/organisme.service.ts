import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Organisme} from "../../../core/models/organisme";

@Injectable({
  providedIn: 'root'
})
export class OrganismeService {
  private url = 'http://localhost:3001/organismes';

  constructor(private http: HttpClient) { }

  getOrganismes(): Observable<Organisme[]> {
    return this.http.get<Organisme[]>(this.url);
  }

}
