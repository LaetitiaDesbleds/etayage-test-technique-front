import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ActivityPageComponent} from "./activity-page.component";

const routes: Routes = [
  {
    path: '',
    component: ActivityPageComponent,
    children: [
      {
        path: 'map', loadChildren:
          () => import('./map/map.module')
            .then((module_) => module_.MapModule)
      },
      {path: '', pathMatch: 'full', redirectTo: 'map'},
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivityPageRoutingModule { }
