import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapRoutingModule } from './map-routing.module';
import {MapComponent} from "./components/map.component";
import {IconsModule} from "../../../icons/icons.module";
import {MatCardModule} from "@angular/material/card";


@NgModule({
  declarations: [
    MapComponent
  ],
    imports: [
        CommonModule,
        MapRoutingModule,
        IconsModule,
        MatCardModule
    ]
})
export class MapModule { }
