import {AfterViewInit, Component} from '@angular/core';
import * as L from 'leaflet';
import {Organisme} from "../../../../core/models/organisme";
import {Coordonnee} from "../../../../core/models/coordonnee";
import {OrganismeService} from "../../organsimes/organisme.service";
import {CoordonneService} from "../../coordonnee/coordonne.service";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements AfterViewInit {
  map!: L.Map;
  organismes: Organisme[] = [];
  coordonnees: Coordonnee[] = [];
  selectedOrganismes: Organisme[] = [];

  smallIcon = new L.Icon({
    iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-icon.png',
    iconRetinaUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-icon-2x.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    shadowSize: [41, 41]
  });

  constructor(private organismeService: OrganismeService, private coordonneService: CoordonneService) {
  }

  /**
   * La méthode ngAfterViewInit est appelée après l'initialisation de la vue du composant
   * Elle initialise la carte en utilisant la méthode createMap()
   * Elle récupère la liste des organismes à partir du service OrganismeService et la liste des coordonnées à partir du service CoordonneService
   * Après avoir récupéré ces données, la méthode addOrganismesToMap() est appelée pour ajouter les organismes à la carte
   * Les appels à getOrganismes() et getCoordonnees() sont imbriqués pour s'assurer que toutes les données nécessaires sont récupérées avant d'essayer d'ajouter les organismes à la carte
   */
  ngAfterViewInit(): void {
    this.createMap();
    this.organismeService.getOrganismes().subscribe((organismes: Organisme[]) => {
      this.organismes = organismes;
      this.coordonneService.getCoordonnees().subscribe((coordonnees: Coordonnee[]) => {
        this.coordonnees = coordonnees;
        this.addOrganismesToMap();
      });
    });
  }

  /**
   * La méthode createMap est utilisée pour initialiser la carte Leaflet
   *
   * Elle définit d'abord les coordonnées de Rennes et le niveau de zoom par défaut pour la carte
   * Ensuite, elle crée une instance de la carte Leaflet et la configure avec les coordonnées de Rennes et le niveau de zoom
   *
   * Elle étermine le niveau de zoom minimum et maximum autorisé pour la carte
   */
  createMap() {
    const rennes = {
      lat: 48.117266,
      lng: -1.6777926
    };

    const zoomLevel = 12;

    this.map = L.map('map', {
      center: [rennes.lat, rennes.lng],
      zoom: zoomLevel
    });

    const mainLayer = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      minZoom: 12,
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    mainLayer.addTo(this.map);/*
    const description = "Description de Rennes"
    this.addMarker(rennes, description);*/

  }

  /**
   * La méthode addMarker est utilisée pour ajouter des marqueurs à la carte
   * Elle prend en entrée les coordonnées sous la forme d'un objet { lat: number, lng: number } et une liste d'organismes
   *
   * Un marqueur est créé aux coordonnées spécifiées avec une icône personnalisée
   * Un écouteur d'événement 'click' est ajouté au marqueur. Lorsqu'un utilisateur clique sur le marqueur,
   * la liste d'organismes associés à ces coordonnées est mise à jour dans la variable selectedOrganismes
   *
   * Puis le marqueur est ajouté à la carte
   */
  addMarker(coords: { lat: number, lng: number }, organismes: Organisme[]) {
    const marker = L.marker([coords.lat, coords.lng], {icon: this.smallIcon});
    marker.on('click', () => {
      this.selectedOrganismes = organismes;
    });
    marker.addTo(this.map);
  }

  /**
   * La méthode addOrganismesToMap est utilisée pour ajouter des organismes à la carte en fonction de leurs coordonnées.
   *
   * Elle crée un objet (coordonneesMap) en utilisant la méthode reduce sur la liste des organismes.
   * L'objet créé regroupe les organismes par coordonnées (latitude et longitude).
   *
   * Une fois que tous les organismes ont été ajoutés à l'objet, elle parcourt les valeurs de l'objet (qui sont des groupes d'organismes avec les mêmes coordonnées)
   * et appelle la méthode addMarker pour chaque groupe, en passant les coordonnées et la liste des organismes pour ce groupe.
   */
  addOrganismesToMap() {
    const coordonneesMap = this.organismes.reduce((map: {
      [key: string]: { lat: number, lng: number, organismes: Organisme[] }
    }, organisme) => {
      const coordonnee = this.coordonnees.find(coordonnee => coordonnee.id === organisme.coordonneeId);
      if (coordonnee) {
        const key = `${coordonnee.latitude},${coordonnee.longitude}`;
        if (!map[key]) {
          map[key] = {
            lat: coordonnee.latitude,
            lng: coordonnee.longitude,
            organismes: [organisme]
          };
        } else {
          map[key].organismes.push(organisme);
        }
      }
      return map;
    }, {});

    Object.values(coordonneesMap).forEach(({lat, lng, organismes}) => {
      this.addMarker({lat, lng}, organismes);
    });
  }

  /**
   * La méthode onBackToTopClick est utilisée pour ramener l'utilisateur au sommet de la page lorsqu'il clique sur l'icon "flèche"
   *
   * Elle utilise la méthode window.scrollTo() de JavaScript qui fait défiler la fenêtre vers une certaine position.=
   * Ici, nous définissons la position cible comme étant le sommet de la page (top: 0)
   *
   * L'option 'smooth' pour le comportement signifie que le défilement sera animé doucement au lieu de sauter directement au sommet
   */
  onBackToTopClick() {
    window.scrollTo({top: 0, behavior: 'smooth'});
  }


}
