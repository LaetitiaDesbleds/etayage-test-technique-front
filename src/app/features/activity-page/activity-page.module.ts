import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActivityPageRoutingModule } from './activity-page-routing.module';
import { ActivityPageComponent } from './activity-page.component';
import {CoreModule} from "../../core/core.module";


@NgModule({
  declarations: [
    ActivityPageComponent
  ],
  imports: [
    CommonModule,
    ActivityPageRoutingModule,
    CoreModule
  ]
})
export class ActivityPageModule { }
