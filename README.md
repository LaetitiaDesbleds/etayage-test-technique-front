# ETAYAGE TEST TECHNIQUE FRONT

Ce projet a été généré avec [Angular CLI](https://github.com/angular/angular-cli) version 16.1.5.

Ceci est la partie front-end du projet de test technique pour Etayage, créée avec **Angular** et **NestJs** pour le back.

# Prérequis

Pour exécuter ce projet, vous devez avoir les logiciels suivants installés sur votre machine :

- Node.js et npm
- Angular CLI
- Docker (optionnel, pour exécuter le projet dans un conteneur Docker)

# Installation

Suivez ces étapes pour installer le projet sur votre ordinateur :

1. Cloner le projet à partir de GitLab avec la commande suivante :
```bash
git clone https://gitlab.com/LaetitiaDesbleds/etayage-test-technique-front.git
```

2. Installer les dépendances du projet avec npm :
```bash
npm install
```

## Build

Lancez la commande suivante pour construire le projet. Les artefacts de construction seront stockés dns le repertoire `dist/`.

```bash
ng build
```

## Exécution

Lancez la commande suivante pour un serveur de développement. Naviguer jusqu'à `http://localhost:4200/`. L'application sera automatiquement rechargée si vous modifiez un fichier sources.
```bash
ng serve
```

## Docker

Lien Docker Hub : 
```bash
https://hub.docker.com/r/laetitiaepsi/etayage_test_technique_front-docker/tags
```

Si vous souhaitez exécuter ce projet dans un conteneur Docker, suivez ces étapes :

1.Construisez l'image Docker avec la commande suivante (la version 1.1 est la plus récente) :

    docker pull laetitiaepsi/etayage_test_technique_front:1.1

2.Exécutez l'image Docker avec la commande suivante :

    docker run -p 8080:80 laetitiaepsi/etayage_test_technique_front:1.1

Ensuite, ouvrez votre navigateur web et visitez http://localhost:8080 pour voir l'application en action dans le conteneur Docker.

## Contact

Si vous avez des questions ou des commentaires sur ce projet,
veuillez me contacter à mon adresse email : **desbleds.laetitia@outlook.fr**
